<?php

namespace Pingpongcms\Menus\Providers;

use Illuminate\Support\ServiceProvider;

class MenusServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../../migrations' => base_path('database/migrations'),
            __DIR__ . '/../../../../public' => public_path('packages/menus'),
        ], 'migrations');

        $this->loadTranslationsFrom(__DIR__.'/../../../resources/lang', 'menus');

        $this->loadViewsFrom(__DIR__.'/../../../resources/views', 'menus');

        require __DIR__ . '/../helpers.php';
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
