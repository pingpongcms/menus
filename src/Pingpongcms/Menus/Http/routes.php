<?php

_resource('menus', 'MenusController');                

Route::group(['middleware' => ['api'], 'prefix' => 'api', 'namespace' => 'Api'], function () {
    Route::get('menus/{id}', [
        'as' => 'api.menus.show',
        'uses' => 'MenusController@show'
    ]);
    Route::post('menus/{id}/update', [
        'as' => 'api.menus.update',
        'uses' => 'MenusController@update'
    ]);
    Route::post('menus/{id}/additem', [
        'as' => 'api.menus.items.add',
        'uses' => 'MenusController@addItem'
    ]);
    Route::post('menus/items/delete', [
        'as' => 'api.menus.items.delete',
        'uses' => 'MenusController@deleteItem'
    ]);
    Route::post('menus/items/{id}/update', [
        'as' => 'api.menus.items.update',
        'uses' => 'MenusController@updateItem'
    ]);
});
