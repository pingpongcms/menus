<?php

namespace Pingpongcms\Menus\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Pingpongcms\Menus\Menu;

class MenusController extends Controller
{
    public function index(Request $request)
    {
        $menus = Menu::filterByQuery($request->q)->paginate(10);

        return view('menus::admin.index', compact('menus'));
    }

    public function edit($id)
    {
        $menu = Menu::findOrFail($id);

        $items = $menu->getItems();

        return view('menus::admin.edit', compact('menu', 'id', 'items'));
    }

    public function create()
    {
        return view('menus::admin.create');
    }

    public function store(Request $request)
    {
        $menu = Menu::create($request->all());

        return redirect()->route('admin.menus.edit', $menu->id);
    }

    public function destroy($id)
    {
        Menu::destroy($id);

        return redirect()->route('admin.menus.index');
    }
}
