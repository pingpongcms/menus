<?php

namespace Pingpongcms\Menus\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Pingpongcms\Menus\Menu;
use Pingpongcms\Menus\MenuItem;

class MenusController extends Controller
{
    public function updateItem(Request $request, $id)
    {
        $item = MenuItem::find($id);

        $item->update($request->all());

        return $item;
    }

    public function deleteItem(Request $request)
    {
        $item = MenuItem::find($request->item_id);

        $item->delete();

        $item->items()->delete();

        return $item;
    }

    public function addItem(Request $request, $id)
    {
        $menu = Menu::findOrFail($id);

        if ($parentId = $request->parent_id) {
            $order = MenuItem::whereParentId($parentId)->count() + 1;
        } else {
            $order = $menu->items()->count() + 1;
        }

        $item = $menu->addItem($request->all() + compact('order'));

        return $item;
    }

    public function update(Request $request, $id)
    {
        $menu = Menu::findOrFail($id);

        $json = json_decode($request->json);

        DB::transaction(function () use ($json) {
            $this->updateMenuItem($json, true);
        });

        return ['status' => true];
    }

    public function updateMenuItem($json, $first = false)
    {
        foreach (array_get($json, 0, []) as $index => $item) {
            $menuItem = MenuItem::find($item->id);

            $data = $first ? ['parent_id' => 0] : [];

            $menuItem->update(['order' => $index] + $data);

            if (isset($item->children)) {
                $childrens = array_get($item->children, 0, []);

                $childrenIds = collect($childrens)->lists('id')->toArray();

                MenuItem::whereIn('id', $childrenIds)->update(['parent_id' => $menuItem->id]);

                if (count($childrens)) {
                    $this->updateMenuItem($item->children);
                }
            }
        }
    }

    public function show(Request $request, $id)
    {
        return MenuItem::findOrFail($id);
    }

}
