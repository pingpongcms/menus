<?php

namespace Pingpongcms\Menus;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'name',
        'description',
    ];

    public function items()
    {
        return $this->hasMany(MenuItem::class);
    }

    public static function make($name, $description = null)
    {
        return static::create(compact('name', 'description'));
    }

    public function getItems()
    {
        return $this->items()->onlyParent()->with('items', 'items.items')->orderBy('order')->get();
    }

    public function addItem($title, $url = null, $permission = null, $order = 0, $icon = null, $target = null)
    {
        $data = is_array($title) ? $title : compact('title', 'url', 'order', 'icon', 'target', 'permission');

        return $this->items()->create($data);
    }

    public function scopeFilterByQuery($query, $q)
    {
        if ($q) {
            $query->where(function ($query) use ($q) {
                $pattern = "%{$q}%";
                $query
                    ->where('name', 'like', $pattern)
                    ->where('description', 'like', $pattern)
                ;
            });
        }
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($menu)
        {
            $menu->items()->delete();
        });
    }
}
