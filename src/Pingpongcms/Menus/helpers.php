<?php

use Pingpongcms\Menus\Menu;

if (!function_exists('menu')) {
    function menu($name)
    {
        return Menu::whereName($name)->firstOrFail()->getItems();
    }
}