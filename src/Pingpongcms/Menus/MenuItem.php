<?php

namespace Pingpongcms\Menus;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $fillable = [
        'menu_id',
        'parent_id',
        'type',
        'title',
        'url',
        'target',
        'icon',
        'order',
        'permission'
    ];

    public function getChilds()
    {
        return $this->items->sortBy('order')->all();
    }

    public function items()
    {
        return $this->hasMany(__CLASS__, 'parent_id');
    }

    public function addItem($title, $url = null, $permission = null, $order = null, $icon = null, $target = null)
    {
        $order = $this->getOrder($order);

        $data = is_array($title) ? $title : compact('title', 'url', 'order', 'icon', 'target', 'permission');

        return $this->items()->create($data);
    }

    private function getOrder($order = null)
    {
        return $order ? $order : $this->items()->count() + 1;
    }

    public function scopeOnlyParent($query)
    {
        return $query->whereParentId(0);
    }

    public function hasChilds()
    {
        return count($this->getChilds());
    }

    public function hasNotChilds()
    {
        return !$this->hasChilds();
    }

    public function addDivider()
    {
        return $this->items()->create([
            'type' => 'divider',
            'title' => '-',
            'order' => $this->getOrder()
        ]);
    }

    public function addHeader($title)
    {
        return $this->addDropdownHeader($title);
    }

    public function addDropdownHeader($title)
    {
        return $this->items()->create([
            'type' => 'dropdown-header',
            'title' => $title,
            'order' => $this->getOrder()
        ]);
    }

    public function accessible()
    {
        if ($permission = $this->permission) {
            $permissions = str_contains($permission, '|') ? explode('|', $permission) : (array) $permission;

            foreach ($permissions as $permission) {
                return auth()->user()->can($permission);
            }

            return false;
        }

        return true;
    }

    public function active()
    {
        return request()->is($this->url);
    }

    public function activeClass($class = 'active')
    {
        return $this->active() ? $class : '';
    }
}
