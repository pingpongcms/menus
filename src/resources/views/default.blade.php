<ul class="{{{ $class or 'nav navbar-nav' }}}">
    @foreach (menu($menu) as $item)
        @if ($item->hasNotChilds())
            @if ($item->accessible())
                <li>
                    <a target="{{ $item->target }}" href="{{ url($item->url) }}">
                        @if ($icon = $item->icon)
                            <i class="{{ $icon }}"></i>
                        @endif
                        {!! $item->title !!}
                    </a>
                </li>
            @endif
        @else
            @if($item->accessible())
                <li class="dropdown">
                    <a href="#!" class="dropdown-toggle" data-toggle="dropdown">
                        {!! $item->title !!}
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        @foreach ($item->getChilds() as $item)
                            @if ($item->accessible())
                                @if ($item->type == 'dropdown-header')
                                    <li class="dropdown-header">
                                        {!! $item->title !!}
                                    </li>
                                @elseif ($item->type == 'divider')
                                    <li class="divider"></li>
                                @else
                                <li>
                                    <a target="{{ $item->target }}" href="{{ url($item->url) }}">
                                        @if ($icon = $item->icon)
                                            <i class="{{ $icon }}"></i>
                                        @endif
                                        {!! $item->title !!}
                                    </a>
                                </li>
                                @endif
                            @endif
                        @endforeach
                    </ul>
                </li>
            @endif
        @endif
    @endforeach
</ul>