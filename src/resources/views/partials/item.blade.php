<li data-id="{{ $item->id }}" id="menuItem{{ $item->id }}">
    <span class="title">{{ $item->title }}</span>
    <div class="menu-actions">
        <a data-toggle="tooltip" title="Add Child Item" parent-id="{{ $item->id }}" parent-title="{{ $item->title }}" class="btn-add-item">
            <i class="fa fa-plus"></i>
        </a>
        <a data-toggle="tooltip" title="Edit Item" href="#!" data-id="{{ $item->id }}" class="edit-menu-item">
            <i class="fa fa-edit"></i>
        </a>
        <a data-toggle="tooltip" title="Remove Item" data-id="{{ $item->id }}" class="btn-remove-item">
            <i class="fa fa-times"></i>
        </a>
    </div>
    @if ($childs = $item->getChilds())
        <ol id="item{{ $item->id }}">
        @foreach ($childs as $item)
            @include('menus::partials.item', compact('item'))
        @endforeach
        </ol>
    @endif
</li>