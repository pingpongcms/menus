@extends('dashboard::layouts.master')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Add New Menu
            <div class="panel-nav pull-right" style="margin-top: -7px;">
                <a href="{!! route('admin.menus.index') !!}" class="btn btn-default">Back</a>
            </div>
        </div>
        <div class="panel-body">
            
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="form-horizontal">
                {!! Form::open(['files' => true, 'route' => 'admin.menus.store']) !!}
                
                <div class="form-group">
                    {!! Form::label('name', 'Name:', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Description:', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-sm-9">
                        {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-2 control-label"></label>
                    <div class="col-sm-9">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>

@stop