@extends('dashboard::layouts.master')

@section('content')
  <div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-8">
                <h3>Menus ({{ $menus->count() }})</h3>
            </div>
            <div class="col-md-3">
                @include('dashboard::partials.search-form')
            </div>
            <div class="col-md-1">
                <a href="{!! route('admin.menus.create') !!}" class="btn btn-default">Add New</a>
            </div>
        </div>
    </div>
    <table class="table table-stripped table-bordered">
        <thead>
            <th class="text-center">#</th>
            <th>Name</th>
            <th>Description</th>
            <th>Created At</th>
            <th class="text-center">Action</th>
        </thead>
        <tbody>
            @foreach ($menus as $index => $user)
                <tr>
                    <td class="text-center">{!! $index + 1 !!}</td>
                    <td>{!! $user->name !!}</td>
                    <td>{!! $user->description !!}</td>
                    <td>{!! $user->created_at !!}</td>
                    <td class="text-center">
                        <div class="btn-group">
                            <a href="{!! route('admin.menus.edit', $user->id) !!}" class="btn btn-sm btn-default" title="Edit" data-toggle="tooltip"><i class="glyphicon glyphicon-edit"></i></a>
                            <a href="#delete{{ $user->id }}" class="btn btn-sm btn-default" title="Delete" data-toggle="modal"><i class="glyphicon glyphicon-remove"></i></a>
                        </div>

                        <div class="modal fade text-left" id="delete{{ $user->id }}">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    {!! Form::open(['method' => 'DELETE', 'route' => ['admin.menus.destroy', $user->id]]) !!}
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title">Delete Menu</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>
                                            Are you sure want to delete this menu?
                                        </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@stop