@extends('dashboard::layouts.master')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Edit Menu
            <div class="panel-nav pull-right" style="margin-top: -7px;">
                <a href="{!! route('admin.menus.index') !!}" class="btn btn-default">Back</a>
            </div>
        </div>
        <div class="panel-body">
            <div id="menuForm">
              <div id="modalAddNewItem" style="display: none;">
                <h3 class="page-header">Add New Item</h3>
                <div class="col-lg-12">
                  <div class="form-group" id="parentWrapper" style="display: none;">
                    <label for="title">Parent Item:</label>
                    <input readonly="readonly" type="text" id="parent" name="parent" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="type">Type:</label>
                    <select name="type" id="type" class="form-control">
                      <option value="item">Menu Item</option>
                      <option value="divider">Divider</option>
                      <option value="dropdown-header">Dropdown Header</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" id="title" name="title" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="url">URL:</label>
                    <input placeholder="http://" type="text" id="url" name="url" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="target">Target:</label>
                    <input type="text" id="target" name="target" class="form-control">
                  </div>
                  <div class="form-group">
                    <label for="icon">Icon:</label>
                    <input placeholder="fa fa-dashboard" type="text" id="icon" name="icon" class="form-control">
                  </div>
                  <div class="form-group">
                    <button class="btn btn-save-add-item btn-primary">Save</button>
                    <button class="btn btn-cancel-add-item btn-default">Cancel</button>
                  </div>
                  </div>
              </div>

            <ol class="nested_with_switch vertical">
              @foreach ($menu->getItems() as $item)
                  @include('menus::partials.item', compact('item'))
              @endforeach
            </ol>
            
            <div class="form-group">
                <button type="button" class="btn btn-save-menu btn-primary">Save</button>
                <a href='#!' parent-id="0" class="btn btn-add-item btn-primary">Add Item</a>
            </div>

        </div>
        </div>
    </div>

@stop

@section('script')
    <script src="https://johnny.github.io/jquery-sortable/js/jquery-sortable-min.js"></script>
    <script>var MENU_ID = {{ $id }};</script>
    <script src="{{ asset('packages/menus/js/all.js') }}"></script>
@stop

@section('style')
    <link rel="stylesheet" href="{{ asset('packages/menus/css/styles.css') }}">
@stop