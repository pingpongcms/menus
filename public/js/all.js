$(function () {
  var group = $("ol.nested_with_switch").sortable();

  $('[data-toggle="tooltip"]').tooltip();

  //** MENU JS**//
  var Menu = {

    menu_id: MENU_ID,
    
    editing: false,

    item_id:0,
    parent_id: 0,
    parent_title: '',
    type: 'item',
    title: '',
    url: '',
    target: '',
    icon: '',

    els: {
      parent: $('input#parent'),
      parentWrapper: $('#parentWrapper'),
      title: $('input#title'),
      url: $('input#url'),
      target: $('input#target'),
      icon: $('input#icon'),
      type: $('input#type'),
      menu: $("ol.nested_with_switch"),
    },

    modal: $('#modalAddNewItem'),

    setType: function (type) {
      this.type = type;
      switch (type) {
        case 'divider':
          this.els.title.val('-');
          this.title = '-';
          this.els.title.prop('disabled', true);
          this.els.url.prop('disabled', true);
          this.els.target.prop('disabled', true);
          this.els.icon.prop('disabled', true);
        break;

        case 'dropdown-header':
          this.els.title.val('');
          this.els.title.prop('disabled', false);
          this.els.url.prop('disabled', true);
          this.els.target.prop('disabled', true);
          this.els.icon.prop('disabled', true);
        break;

        default:
          this.els.title.val('');
          this.els.title.prop('disabled', false);
          this.els.url.prop('disabled', false);
          this.els.target.prop('disabled', false);
          this.els.icon.prop('disabled', false);
        break;
      }
    },

    add: function (parent_id, parent_title) {
      this.parent_id = parent_id;
      this.parent_title = parent_title;

      this.setParent();
      this.showModal();
    },

    edit: function (id) {
      this.editing = true;
      var url = App.dashboardUrl + '/api/menus/'+id;
      Vue.http.get(url, function (res) {
        this.parent_id = res.parent_id;
        this.item_id = res.id;
        this.title = res.title;
        this.url = res.url;
        this.target = res.target;
        this.icon = res.icon;

        this.fill();
        this.showModal();
      }.bind(this));
    },

    fill: function () {
      this.els.title.val(this.title);
      this.els.url.val(this.url);
      this.els.target.val(this.target);
      this.els.icon.val(this.icon);
    },

    setParent: function () {
      if (this.parent_id && this.parent_id !== '0' && this.parent_id !== 0) {
        this.els.parent.val(this.parent_title);
        this.els.parentWrapper.show();
      } else {
        this.els.parentWrapper.hide();
      }
    },

    showModal: function () {
      this.modal.show();
    },

    hideModal: function () {
      this.modal.hide();
    },

    reset: function () {
      this.parent_id = 0;
      this.parent_title = '';
      this.title = '';
      this.url = '';
      this.target = '';
      this.icon = '';

      this.els.title.val('');
      this.els.url.val('');
      this.els.target.val('');
      this.els.icon.val('');
      this.els.parent.val('');
    },

    cancel: function () {
      this.reset();
      this.hideModal();
    },

    prepareData: function () {
      this.title = this.els.title.val();
      this.url = this.els.url.val();
      this.target = this.els.target.val();
      this.icon = this.els.icon.val();
    },

    save: function () {
      if (this.editing == true) {
        this.update()
      } else {
        this.store();
      }
    },

    store: function () {
      this.prepareData();

      var url = App.dashboardUrl + '/api/menus/'+this.menu_id+'/additem';
      var data = this.getData();

      Vue.http.post(url, data, function (res) {
        this.pushItem(res);
        this.reset();
        this.hideModal();
      }.bind(this));
    },

    getData: function () {
      return {
        _token: App.token,
        menu_id: this.menu_id,
        parent_id: this.parent_id,
        type: this.type,
        title: this.title,
        url: this.url,
        target: this.target,
        icon: this.icon,
      };
    },

    update: function () {
      this.prepareData();

      var url = App.dashboardUrl + '/api/menus/items/'+this.item_id+'/update';
      var data = this.getData();

      Vue.http.post(url, data, function (res) {
        this.updateItem(res);
        this.reset();
        this.hideModal();
      }.bind(this));

      this.editing = false;
    },

    updateItem: function (res) {
      $('#menuItem'+res.id+' > span.title').text(res.title);
    },

    remove: function (id) {
      var url = App.dashboardUrl + '/api/menus/items/delete';
      var data = {
        _token: App.token,
        item_id: id
      };
      Vue.http.post(url, data, function (res) {
        $('#menuItem'+id).remove();
      }.bind(this));
    },

    pushItem: function (item) {
      var tpl = '<li id="menuItem'+item.id+'" data-id="'+item.id+'"><span class="title">'+item.title+'</span>\
        <div class="menu-actions">\
          <a data-toggle="tooltip" title="Add Child Item" href="#!" class="btn-add-item" parent-id="'+item.id+'" parent-title="'+item.title+'">\
              <i class="fa fa-plus"></i>\
          </a>\
          <a data-toggle="tooltip" title="Edit Item" href="#!" class="edit-menu-item" data-id="'+item.id+'">\
              <i class="fa fa-edit"></i>\
          </a>\
          <a data-toggle="tooltip" title="Remove Item" href="#!" class="btn-remove-item" data-id="'+item.id+'">\
              <i class="fa fa-times"></i>\
          </a>\
        </div>\
        <ol id="item'+item.id+'"></ol>\
      </li>';
      var target;
      if (item.parent_id == '0' || item.parent_id == 0) {
        target = this.els.menu;
      } else {
        target = this.els.menu.find('#item'+item.parent_id);
      }

      target.append(tpl);
      target.find('[data-toggle="tooltip"]').tooltip();
    }
  }
  
  $(document).on('click', '.btn-add-item', function () {
    var t = $(this);
    Menu.add(t.attr('parent-id'), t.attr('parent-title'));
  });

  $(document).on('click', '.btn-cancel-add-item', function () {
    var t = $(this);
    Menu.cancel();
  });

  $(document).on('click', '.btn-remove-item', function () {
    var t = $(this);
    Menu.remove(t.data('id'));
  });

  $(document).on('click', '.edit-menu-item', function () {
    var t = $(this);
    Menu.edit(t.data('id'));
  });

  $(document).on('click', '.btn-save-add-item', function () {
    var t = $(this);
    Menu.save();
  });

  $(document).on('click', '.btn-save-menu', function () {
    var t = $(this);
    var data = group.sortable("serialize").get();
    var jsonString = JSON.stringify(data, null, ' ');
    var url = App.dashboardUrl + '/api/menus/'+MENU_ID+'/update';
    var data = {
      _token: App.token,
      json: jsonString
    }
    t.button('loading');
    Vue.http.post(url, data, function (res) {
      t.button('reset');
    });
  });

  $('select#type').on('change', function () {
    var type = $(this).val();
    Menu.setType(type);
  });

});
